var path = require('path');
var copydir = require('copy-dir');

var exclude = ['.scss', '.js']

var fromPath = path.join(__dirname, 'src')
var toPath = path.join(__dirname, 'build')

copydir.sync(fromPath, toPath, function(stat, filepath, filename){
  if(stat === 'file' && exclude.indexOf(path.extname(filepath)) != -1) {
    return false;
  }
  return true;
}, function(err){
  console.log('Failed to copy!');
});
