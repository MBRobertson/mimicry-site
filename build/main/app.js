'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();

app.set('view engine', 'pug');
app.set('views', _path2.default.join(__dirname, 'views'));

app.get('/page/:title', function (req, res) {
    //res.send(req.params);
    //console.log(req.params.title);
    var page = req.params.title;
    page = page.replace('-', '/');
    res.render(_path2.default.join('page', page), {});
});

app.use(_express2.default.static(_path2.default.join(__dirname, 'client')));

app.get('/*', function (req, res) {
    res.render('index', {});
});

exports.default = app;