'use strict';

/* global $, history */

function run() {
    $.ajax({
        url: '/page/projects',
        context: $('.content')
    }).done(function (data) {
        $(this).html(data);
    });
}

var pageId = 0;

function page(name, overrideState, backward) {
    pageId++;
    name = name.replace('/', '');
    if (overrideState || overrideState == undefined) history.pushState({ page: name, id: pageId }, name, '/' + name);

    if (name == '' || name == '/') {
        name = 'index';
    }
    //Show loading
    $.ajax({
        url: '/page/' + name,
        context: $('.content')
    }).done(function (data) {
        swapPage(data, backward);
    }).fail(function (err) {
        hideProgress();
        console.log("Handle the error or something");
    });
    showProgress();
    setProgress(90);
    var oldKeyword = "content-post";
    if (backward) oldKeyword = "content-pre";
    var origC = $('.content');
    origC.addClass(oldKeyword);
}

var progress = void 0;

function showProgress() {
    progress.addClass('progress-show');
}

function setProgress(percent) {
    progress.css('width', percent + '%');
}

function hideProgress() {
    progress.removeClass('progress-show');
    setTimeout(function () {
        setProgress(0);
    }, 200);
}

function swapPage(content, backward) {
    var newKeyword = "content-pre";
    var oldKeyword = "content-post";
    if (backward == true) {
        newKeyword = "content-post";
        oldKeyword = "content-pre";
    }
    var contentHolder = $('main');
    var origC = $('.content');
    var newC = $('<section class="content ' + newKeyword + '"></section>');
    newC.html(content);

    var page = newC.find('.data');
    document.title = page.data('title') + ' - Mimicry';

    origC.addClass(oldKeyword);
    newC.appendTo(contentHolder);
    setTimeout(function () {
        newC.removeClass(newKeyword);
    }, 25);
    setTimeout(function () {
        origC.remove();
    }, 350);

    setProgress(100);
    hideProgress();
}

window.onpopstate = function (event) {
    var id = event.state.id;
    var backward = false;
    if (id < pageId) backward = true;
    pageId = id;
    page(event.state.page, false, backward);
    var loc = window.location.pathname;
    $('.active').removeClass('active');
    $('a[data-page="' + loc + '"]').parent().addClass('active');
};

$(document).ready(function () {
    var loc = window.location.pathname;
    progress = $('.progress');
    setProgress(0);
    page(window.location.pathname, false);
    $('.active').removeClass('active');
    $('a[data-page="' + loc + '"]').parent().addClass('active');

    history.replaceState({ page: window.location.pathname, id: pageId }, window.location.pathname, window.location.pathname);

    $('.button>a').click(function () {
        var link = $(this);
        if (!link.parent().hasClass('active')) {
            $('.active').removeClass('active');
            var newPage = link.data('page');
            link.parent().addClass('active');
            page(newPage);
        }
    });
});