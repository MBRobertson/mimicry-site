import express from 'express'
import main from './main/app'

let app = express();

//Routing logic
app.use('/', main);

//Start the server
let port = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8080
let ip = process.env.OPENSHIFT_NODEJS_IP || process.env.IP || '127.0.0.1'

let server = app.listen(port, ip, () => {
  console.log('App listening on port %s', server.address().port);
  console.log('Press Ctrl+C to quit.');
});
