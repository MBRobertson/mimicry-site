import express from 'express'
import path from 'path'

let app = express()

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'))

app.get('/page/:title', (req, res) => {
    //res.send(req.params);
    //console.log(req.params.title);
    let page = req.params.title;
    page = page.replace('-', '/');
    res.render(path.join('page', page), {});
});

app.use(express.static(path.join(__dirname, 'client')))

app.get('/*', (req, res) => {
    res.render('index', {});
});

export default app
