/* global $, history */

function run() {
    $.ajax({
        url: '/page/projects',
        context: $('.content')
    }).done(function(data) {
       $(this).html(data);
    });
}

let pageId = 0;

function page(name, overrideState, backward) {
    pageId++;
    name = name.replace('/', '');
    if (overrideState || overrideState == undefined)
        history.pushState({ page: name, id: pageId }, name, '/' + name)

    if (name == '' || name == '/') {
        name = 'index';
    }
    //Show loading
    $.ajax({
        url: '/page/' + name,
        context: $('.content')
    }).done(function(data) {
       swapPage(data, backward);
    }).fail(function(err) {
       hideProgress();
       console.log("Handle the error or something");
    });
    showProgress();
    setProgress(90);
    let oldKeyword = "content-post";
    if (backward) oldKeyword = "content-pre";
    let origC = $('.content');
    origC.addClass(oldKeyword);
}

let progress;

function showProgress() {
    progress.addClass('progress-show');
}

function setProgress(percent) {
    progress.css('width', percent + '%');
}

function hideProgress() {
    progress.removeClass('progress-show');
    setTimeout(() => {
        setProgress(0);
    }, 200);
}

function swapPage(content, backward) {
    let newKeyword = "content-pre";
    let oldKeyword = "content-post";
    if (backward == true) {
        newKeyword = "content-post";
        oldKeyword = "content-pre";
    }
    let contentHolder = $('main');
    let origC = $('.content');
    let newC = $('<section class="content ' + newKeyword + '"></section>');
    newC.html(content);

    let page = newC.find('.data');
    document.title = page.data('title') + ' - Mimicry';

    origC.addClass(oldKeyword);
    newC.appendTo(contentHolder);
    setTimeout(() => { newC.removeClass(newKeyword) }, 25);
    setTimeout(() => {
        origC.remove();
    }, 350);

    setProgress(100);
    hideProgress();
}

window.onpopstate = function(event) {
    let id = event.state.id;
    let backward = false;
    if (id < pageId)
        backward = true;
    pageId = id;
    page(event.state.page, false, backward);
    let loc = window.location.pathname
    $('.active').removeClass('active');
    $('a[data-page="'+loc+'"]').parent().addClass('active');
}

$(document).ready(function() {
    let loc = window.location.pathname
    progress = $('.progress');
    setProgress(0);
    page(window.location.pathname, false);
    $('.active').removeClass('active');
    $('a[data-page="'+loc+'"]').parent().addClass('active');

    history.replaceState({ page: window.location.pathname, id: pageId }, window.location.pathname, window.location.pathname);

    $('.button>a').click(function() {
        let link = $(this);
        if (!link.parent().hasClass('active')) {
            $('.active').removeClass('active');
            let newPage = link.data('page');
            link.parent().addClass('active');
            page(newPage);
        }
    });
});
